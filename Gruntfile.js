module.exports = function ( grunt ) {
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.initConfig( {
		sprite:{
            all: {
                src: 'grunt/wiki/sprites/*.png',
                dest: 'wiki/front_images/spritesheet.png',
                imgPath: '/front_images/spritesheet.png',
                destCss: 'wiki/css/sprites.css'
            }
        },
		less: {
            modern: {
                options: {
                    paths: [],
                    relativeUrls: false
                },
                files: {
                    'wiki/css/styles.css': [
                    	'grunt/wiki/styles.less'
                    ]
                }
            },
            ieLESS: {
                options: {
                    paths: [],
                    relativeUrls: true
                },
                files: {
                    'wiki/css/ie.css': [
                        'grunt/wiki/ie.less'
                    ]
                }
            }
        },
        cssmin: {
            options: {
                sourceMap: true
            },
            css: {
                src: [
                	//'custom/jquery-ui.min.css',
                    //'custom/jquiautocompleteonly/jquery-ui.min.css',
                    // 'custom/jquiautocompleteonly/jquery-ui.css',
                	'wiki/css/styles.css'
                ],
                dest: 'wiki/css/styles.min.css'
            },
            ieMIN: {
                src: 'wiki/css/ie.css',
                dest: 'wiki/css/ie.min.css'
            }
        },
        concat: {
            options: {
               stripBanners: {all: true},
                /*banner: '/!* grunt' +
                    'js *!/',*/
                separator: ';' 
            },
            dist: {
                src: [
                    'grunt/wiki/js/jquery-3.1.0.js',
                    'grunt/wiki/js/jquiautocompleteonly/jquery-ui.js',
                    'grunt/wiki/js/jquery.tmpl.min.js',
                    'grunt/wiki/js/jqscripts.js',
                    'grunt/wiki/js/scripts.js'
                ],
                dest: 'wiki/js/stacked.js'
            }
        },
        uglify: {
            options: {
                mangle: false, /* минификация названий функций и переменных */
                sourceMap: true,
                includeSources: true
            },
            js: {
                src: [
                	'wiki/js/stacked.js'
                ],
                dest: 'wiki/js/stacked.min.js'
            }
        },
        watchfiles: {
            less: {
                files: [
                    'grunt/wiki/styles.less',
                    'grunt/wiki/extends/*.less',
                    'grunt/wiki/variables.less',
                    'grunt/wiki/ie.less'
                ],
                tasks: ['less','cssmin'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },
            uglify: {
            	files: [
                    'grunt/wiki/js/jqscripts.js',
                    'grunt/wiki/js/scripts.js'
                ],
                tasks: ['concat','uglify']
            }
        }
	} );

	grunt.renameTask('watch',
	    'watchfiles');
	grunt.registerTask('watch', [
	    'sprite',
	    'less',
	    'cssmin',
        'concat',
	    'uglify',
	    'watchfiles'
	]);
	grunt.registerTask( 'default', [
		'sprite',
		'less',
		'cssmin',
        'concat',
		'uglify',
		// 'watchfiles'
	] );
	
	grunt.registerTask( 'js', ['concat','uglify']);
    grunt.registerTask( 'sprites', ['sprite','less','cssmin'] );

};
