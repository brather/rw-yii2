<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 09.06.2017
 * Time: 17:58
 */

namespace app\assets;

use yii\web\AssetBundle;

class ErrorAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $js = [
        '/js/stacked.min.js',
    ];
    public $css = [
        '/css/styles.min.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}