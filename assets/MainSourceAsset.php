<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 09.06.2017
 * Time: 17:58
 */

namespace app\assets;

use yii\web\AssetBundle;

class MainSourceAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@wiki';
    public $js = [
        'js/stacked.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/gallery-image.js',
    ];
    public $css = [
        'css/styles.min.css',
        'css/magnific-popup.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

    public function __construct( array $config = [] )
    {
        parent::__construct( $config );

    }
}