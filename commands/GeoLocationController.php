<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 18.07.2017
 * Time: 10:55
 */

namespace app\commands;


use app\models\wiki\GeoObject;
use app\models\wiki\GeoObjectQuery;
use app\models\wiki\Page;
use yii\console\Controller;

class GeoLocationController
    extends Controller
{
    public $saveAll = 0;
    public $withProxy = 1;
    /**
     * $saveAll = 0  - по умолчанию будет записываться первая релевантная запись
     * $saveAll = 1  - вся выдача запишеться в таблицу
     **/
    public function actionIndex() {

        echo "Search item on DB\n";
        $storageToponyms = Page::find()
            ->select("page.page_id, page.page_main_name")
            ->leftJoin("eto_geo_object", 'page.page_id = eto_geo_object.page_id')
            ->where([
                'page_category' => 2,
                'eto_geo_object.page_id' => null,
            ])
            ->all();
        echo "Find item: " .count($storageToponyms) ."\n";

        $timeStart = time();
        $itemSave = 0;
        $itemProc = 0;
        foreach ($storageToponyms as $page) {
            /* Заменим все _ на "пробелы" */
            $page->page_main_name = str_replace('_', ' ', $page->page_main_name);

            /* если $saveAll = 0, проверим page_id в таблице geo_location */
            if ($this->saveAll == 0 && GeoObject::findOne(['page_id' => $page->page_id]) !== null) continue;

            $osmResult = self::getData($page->page_main_name, $this->withProxy);

            if (count($osmResult) < 1) {
                /* Уберем все слова в скобочках если результат пустой */
                $page->page_main_name = preg_replace('/(.+)(\s\(.+)/i', '$1', $page->page_main_name);
                $osmResult = self::getData($page->page_main_name, $this->withProxy);
            }

            /* после очистки пустой результат, идем к следующему топониму */
            if (count($osmResult) < 1) continue;

            if (isset($osmResult['error'])) {
                echo "\nError: " . $osmResult['error'] . "\n";
                break;
            }

            foreach ($osmResult as $place) {

                $checkPlace = GeoObject::findOne(['place_id' => $place->place_id]);

                /* если такая запись уже есть, пропускаем её */
                if($checkPlace !== null) continue;

                $newPlace               = new GeoObject();
                $newPlace->place_id     = $place->place_id;
                $newPlace->page_id      = $page->page_id;
                $newPlace->lat          = $place->lat;
                $newPlace->lon          = $place->lon;
                $newPlace->display_name = $place->display_name;
                $newPlace->class        = $place->class;
                $newPlace->type         = $place->type;
                $newPlace->importance   = $place->importance;
                $newPlace->save();

                $itemSave++;

                /* Запишем только первую запись */
                if ($this->saveAll == 0) break;
            }

            $itemProc++;

            if (($timeStart + 10) < time() ){
                $timeStart = time();
                echo "Processed toponyms: " . ($itemProc) . "\r";
            }
        }

        echo "\nSave places on DB: " . $itemSave . "\n";

        //$geoObjectModel = GeoObject::findOne()

        return 0;
    }

    /* Запуск через прокти по-умолчанию */
    private static function getData($name, $proxy = 1) {
        $param = [
            'q' => $name,
            'accept-language' => 'ru',
            'format' => 'json'
        ];
        $proxyArr = [];
        $f = fopen(__DIR__ . '/proxy_list', 'r');

        while($lstr = fgets($f)){
            $proxyArr[] = $lstr;
        }
        fclose($f);

        /*$proxyArr = [
                "104.154.205.214:1080", "83.243.66.120:9050", "46.105.107.107:30435", "104.236.238.10:1080",
                "92.240.248.118:1080", "178.62.171.190:41429",
                "139.59.99.101:1080","159.203.8.92:1080", "139.59.109.156:1080","139.59.10.98:1080",
                "138.197.145.103:1080","207.154.231.211:1080",
                "159.203.152.83:1080","138.68.161.60:1080","139.59.101.223:1080",
                "46.105.107.107:28057","188.166.83.13:1080","159.203.161.144:1080",
                "138.68.161.14:1080","46.105.107.107:55155",
                "94.177.216.236:2016","207.154.231.210:1080","159.203.166.41:1080",
                "139.59.202.232:1080","207.154.231.208:1080","162.243.107.120:1080",
                "173.230.59.233:45454","67.205.146.77:1080",
                "162.243.78.25:1080","94.177.216.229:2016","138.68.24.145:1080","138.68.37.49:1080",
                "147.135.209.190:1080","92.222.180.156:1080","66.232.174.93:45454",
                "94.177.216.16:2016","138.197.157.68:1080","188.226.141.219:1080","138.68.240.218:1080",
                "66.253.249.61:45454","178.151.244.207:45454","138.197.58.55:1080",
                "188.226.141.127:1080","46.105.107.107:54823",
                "138.197.157.60:1080","94.177.216.66:2016","162.243.108.161:1080","139.59.163.171:1080",
            ];*/

        $ch = curl_init("http://nominatim.openstreetmap.org/search?" . http_build_query($param));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=UTF-8']);

        /* Запуск через прокти ПО АРГУМЕНТУ */
        if ($proxy == 1 && count($proxyArr)) {
            /* count array mast 1 or more, random not work correct ik count = 1 */
            $key = (count($proxyArr) < 2) ? 0 : rand() % count($proxyArr);

            $p = $proxyArr[$key];
            curl_setopt($ch, CURLOPT_PROXY, $p);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
        }

        usleep(500000);

        $j = curl_exec($ch);

        $err = curl_error($ch);

        if ($proxy == 1 && count($proxyArr) && $err !== '') {
            $err .= " Proxy is: " . $p;
        }

        curl_close($ch);

        return ($err == '') ? json_decode($j) : ['error' => $err];
    }

    public function optionAliases()
    {
        return [
            'p' => 'withProxy',
            'a' => 'saveAll',
            'with-proxy' => 'withProxy',
            'save-all' => 'saveAll',
        ];
    }

    public function options( $actionID )
    {
        return ['withProxy', 'saveAll'];
    }
}