<?php

namespace app\components;

/**
 * This is just an example.
 *
 * @property null|ElasticHelper inst
*/
class ElasticHelper
{
    private $host = null;

    private $urls = [
        'page' => '/articles/text?',
        'random' => '/titles/random',
        'search' => '/titles/search?',
        'fts' => '/search/fts?'
    ];

    public $opt;

    private static $_instance = null;

    private function __construct( $opt )
    {
        $this->host = (YII_ENV === 'dev')  ? 'http://185.98.83.176/api' : 'http://172.16.62.11:4080';
        $this->opt = $opt;
    }

    protected function __clone()
    {
        // ограничивает клонирование объекта
    }

    static public function init($opt)
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self($opt);
        }

        self::$_instance->opt = $opt;

        return self::$_instance;
    }

    public function run()
    {
        $opt = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json; charset=UTF-8'
            ),
            CURLOPT_CONNECTTIMEOUT => 2
        ];
        $url = $this->urls[$this->opt['url']];
        $data = [];
        $rq = \Yii::$app->request->get();

        if ($this->opt['url'] === 'page')
        {
            $url .= "full_title=" . $rq['suffix'] .":". $rq['title'];
            /* Категории может и не быть */
            $url .= (isset($rq['category']) && !empty($rq['category']) ) ? "/".$rq['category']: "";

        } else if ($this->opt['url'] === 'search')
        {
            $url .= http_build_query($this->opt['params']);
        } else if ($this->opt['url'] === 'random')
        {
            $url .= "?limit=3&illustrated=1";
        } else if ($this->opt['url'] === 'fts')
        {
            $url .= http_build_query($this->opt['params']);
        }

        return  self::_curlRun($this->host . $url, $data, $opt);
    }

    static private function _curlRun ($url, $data, $opt = []) {
        // Setup cURL
        $ch = curl_init($url);
        if (count($opt))
        {
            foreach ($opt as $code => $val)
                curl_setopt($ch, $code, $val);
        }
        else
        {
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json; charset=UTF-8'
                ),
                CURLOPT_POSTFIELDS => json_encode($data)
            ));
        }

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if ($response === FALSE) {
            die(curl_error($ch));
        }

        // Decode the response
        return json_decode($response, TRUE);
    }
}
