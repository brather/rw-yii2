<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 11.07.2017
 * Time: 17:20
 */

namespace app\components\rw;
use app\models\wiki\SecIpBlackList;

/**
 * Предопределено что обновления происходят раз в 10 суток.
 */
class BlackListUpdate
{
    /* Constants */
    const BLACK_LIST_URL = 'https://myip.ms/files/blacklist/general/latest_blacklist.txt';
    const BLACK_LIST_TABLE = 'sec_ip_black_list';

    /* Static variables */
    static private $t;

    /* Private variables*/
    private $db;
    private $file;
    private $option;
    private $insertData;
    private $needToUpdate;

    /* Construction of object*/
    public function __construct()
    {
        return $this;
    }

    /* Initialisation option */
    public function initOption()
    {
        $this->option = SecurityOption::getOption('LAST_UPDATE_BLACK_LIST');

        return $this;
    }

    /**
     *  Нужно ли обновлять данные ?
     *  true    - Yes
     *  false   - No
     *
     * @return bool
     */
    public function needToUpdate()
    {
        /*  Раз в 10 суток
            Если время последнего обновления больше времени сейчас минус 10 суток */
        return ($this->option['LAST_UPDATE_BLACK_LIST'] < (time() - (60*60*24*10)));
    }

    public function parseFileData()
    {
        $i = 0;
        $ar = [];
        $fw = fopen(self::BLACK_LIST_URL, 'r');

        while ($ch = fgets($fw)){

            if (substr($ch, 0, 1) === "#" || substr($ch, 0, 1) === "\n")
                continue;

            preg_match("/((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\t{3}\s?\#\s?(\d{4}-\d{2}-\d{2}),\s([\w\d\-\.]+),\s(\w+),\s(\d+)/m", $ch, $ar[]);

            if (1 >= count($ar[$i]))
                unset($ar[$i]);
            else
                $ar[$i][0] = self::ipToInt($ar[$i][1]);

            $i++;
        }

        fclose($fw);

        $this->insertData = $ar;
    }

    public function prepareToInsert()
    {
        foreach ($this->insertData as $item) {
            $obInsert = new SecIpBlackList();
            $obInsert->INT_IP        = $item[0];
            $obInsert->CREATE_DATE   = date("Y-m-d H:i:s", strtotime($item[2]));
            $obInsert->HOST          = $item[3];
            $obInsert->COUNTRY_ID    = $item[4];
            $obInsert->BLACK_LIST_ID = $item[5];

            if (!$obInsert->insert()){
                $error = $obInsert->getErrors();
            }
        }

        $this->updateLastUpdate();

        $this->insertData = null;
    }

    private function updateLastUpdate()
    {
        SecurityOption::setOption(['NAME' => 'LAST_UPDATE_BLACK_LIST', 'VALUE' => time()]);
    }


    private function notInDB()
    {
        $cond = [];
        foreach ($this->insertData as $item)
            $cond[] = $item[0];

        $db = SecIpBlackList::findAll(['in', 'INT_IP', $cond]);

        /* Если добавляемый IP уже есть в БД удаляем его что бы не записывать в БД повторно */
        foreach ($db as $row ) {
            foreach ($this->insertData as $k => $item)
                if ($item[0] == (int)$row->INT_IP)
                    unset($this->insertData[$k]);
        }
        unset($db, $row, $cond);
    }

    /**
     * @param $arIpInt
     *
     * @return array|bool|int
     */
    public static function ipToInt($arIpInt)
    {
        $res = false;

        if (is_array($arIpInt)) {

            $res = [];
            foreach ($arIpInt as $ip) {
                $arIP = explode(".", $ip);

                foreach ($arIP as &$int)
                    $int = $int * 1;

                $intIP = ($arIP[0] << 24) + ($arIP[1] << 16) + ($arIP[2] << 8) + $arIP[3];

                $res["INT_IP"][] = $intIP;
            }
        }

        if (is_string($arIpInt)) {

            $arIP = explode(".", $arIpInt);

            foreach ($arIP as &$int)
                $int = (int)$int;

            $res = ($arIP[0] << 24) + ($arIP[1] << 16) + ($arIP[2] << 8) + $arIP[3];
        }
        return $res;
    }

    /* Start cron operation with this method */
    static public function syncBlackList()
    {
        if (!self::$t)
            self::$t = new BlackListUpdate();

        $bl = self::$t->initOption()->needToUpdate();

        if ($bl === true) {
            self::$t->parseFileData();
            self::$t->notInDB();
            self::$t->prepareToInsert();
        }
    }

    static function testParseText() {
        $t = new BlackListUpdate();
        $t->parseFileData();

        $arIP = [];

        foreach ($t->insertData as $item) {
            $arIP[] = $item[1];
        }

        self::ipToInt($arIP);
    }
}