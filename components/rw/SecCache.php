<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 12.07.2017
 * Time: 15:00
 */

namespace app\components\rw;

use yii\caching\ApcCache;

class SecCache extends ApcCache {
    public $useApcu = true;
}

