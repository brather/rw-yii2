<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 11.07.2017
 * Time: 13:00
 */
namespace app\components\rw;

use app\models\wiki\SecIp;
use app\models\wiki\SecIpBlackList;
use app\models\wiki\SecWhiteList;
use yii\db\Query;
use yii\web\Cookie;

class Security
{
    const IP_TABLE = "sec_ip";
    const IP_BLACK_LIST_TABLE = "sec_ip_black_list";
    const IP_OPTION_TABLE = "sec_option";

    private $sessionName;
    private $session_id = "";
    private $db;
    private $option;
    private $ipData;

    public function __construct()
    {
        $this->sessionName = "sec_session";

        /* Есть ли ID Сессии */
        $this->session_id = session_id();

        /* Запустим сессию если она не запускалась */
        if (empty($this->session_id)) {
            session_start();
            $this->session_id = session_id();
        }

        /* Проведем, если необходимо, обновление "Черного листа" */
        BlackListUpdate::syncBlackList();

        $this->getRealIP();

        /* Проверим установку сессии */
        if (empty($this->session_id) || $this->ipData === false) {
            $this->breakBot();
        }

        $cookie = \Yii::$app->request->cookies;

        if (!$cookie->getValue($this->sessionName) || empty($cookie->getValue($this->sessionName))) {
            \Yii::$app->response->cookies->add( new Cookie([
                    'name' => $this->sessionName,
                    'value' => $this->session_id,
                    'expire' => time() + (60*60*24*30),
                    'path' => "/"
                ])
            );
        }

        $this->option = SecurityOption::getOption();

        /* IP в белом листе, не будем проводить проверки */
        if (!$this->inWhiteList()) {

            //BlackListUpdate::testParseText();

            /* проверка перед вызовом здесь */
            /* проверка на наличие IP в черном списке */
            if ( $this->checkStepOne() === false )
                $this->breakBot();

            /* проверка */
            if ( $this->checkStepTwo() === false )
                $this->breakBot();

            /* Проверяется в последнюю очеред, когда первые данные уже записались в БД */
            if ( $this->maxConnectFromSingleIP($this->ipData['INT_IP']) === false )
                $this->breakBot();
        }

        /* конец проверкам */
        /* если не попало ни в один из блоков, идем дальше и отображаем страницу пользователю */
    }

    /**
     * Начало события ☯
     */
    public static function onBeforePageDisplay( )
    {
        /* ►Вся магия начинается здесь◄ ☯ */
        $s = new self(/*☺*/);
    }

    private function breakBot($page = 'blocked') {

        /* если защита сработает */
        while (ob_get_level() > 0)
            $this->clearBuffer();

        if ($page === false) exit(0); // для теста, просто очистим буфер и выйдем

        /* Ds,hfcsdftv  */
        throw new \yii\web\MethodNotAllowedHttpException("Blocked", 403);
    }

    /**
     * nginx Работает как прослойка между Клиентом и http
     * он передает заголовок HTTP_X_REAL_IP в массив $_SERVER
     * это и будет реальным IP клиента.<br><br>
     *
     * Эта настройка есть в nginx -  proxy_set_header        X-Real-IP $remote_addr;
     * без нее будет все время срабатывать защина и не пускать никого на сайт.<br><br>
     *
     * Сформируем массив с двумя значениями,  IP ХХХ.ХХХ.ХХХ.ХХХ
     * и значение IP вычесленное как целое число.<br><br>
     *
     */
    private function getRealIP () {

        $xRealIP = isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $_SERVER['REMOTE_ADDR'];

        /* False if not set Real IP client */
        if (!$xRealIP) $this->ipData = false;

        $arIP = explode(".", $xRealIP);

        foreach ($arIP as &$int)
            $int = (int)$int;

        $tI = microtime(true);

        // for ($i = 0; $i < 10000000; $i++) // todo test
        $intIP = ($arIP[0] << 24) + ($arIP[1] << 16) + ($arIP[2] << 8) + $arIP[3];

        $tI = microtime(true) - $tI;

        $this->ipData = array(
            "INT_TIME" => $tI,
            "INT_IP" => $intIP,
            "REAL_IP" => $xRealIP
        );
    }

    /**
     * @return false|string
     */
    private function getUserAgent() {

        if (empty($_SERVER['HTTP_USER_AGENT'])) return false;

        return $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * Когда защита сработает, очищаем весь буффер
     */
    private function clearBuffer ()
    {
        ob_flush();
        ob_end_clean();
    }

    /**
     * Check IP to inner i black list from DB
     *
     *
     * @return bool
     *
     * Return false if IP in BlackList
     * Return true if IP is not in BlackList? continue on stepTwo
     */
    private function checkStepOne()
    {
        $res = SecIpBlackList::findOne(['INT_IP' => $this->ipData['INT_IP']]);

        return ( isset($res->ID) && $res->ID > 0 ) ? false : true ;
    }

    /**
     * Check on table and most set Cookie,
     * Insert into Black List if suspicion
     *
     * @return bool
     *
     */
    private function checkStepTwo()
    {
        $row = SecIp::findOne(["INT_IP" => $this->ipData['INT_IP'], "SESSION_ID" => $this->session_id]);

        /* Есть уже такая запись на подобное подключение есть */
        if (is_object($row)) {
            // todo По какой то причине база возвращает все как текст !!! WTF
            $row->ID = (int)$row->ID;
            $row->TIME_POINT = (int)$row->TIME_POINT;
            $row->CONN_PER_TIME = (int)$row->CONN_PER_TIME;
            $row->INT_IP = (int)$row->INT_IP;
            $time = time(); // time now

            /*  запись о посещении с этого IP и подобным SESSION_ID уже есть в таблицах,
                значит наличие куки так же можно проверить,
                если не вернется true значит куки не удалось установить,
                выведем соответсвующее предупреждение */
            if ($this->checkCookie() === false)
                $this->breakBot('cookie');

            /* Точка отсчета   */
            if ( $row->TIME_POINT >= ($time - $this->option['MAX_TIME_REQUEST']) ) {

                if ( 0 >= ($this->option['MAX_REQUECT_PER_TIME'] - $row->CONN_PER_TIME) ) {

                    /* Записываем данного пользователя в Черный лист */
                    $this->writeToBlackList([
                        "IP_ID" => $row->ID,
                        "INT_IP" => $row->INT_IP
                    ]);
                    /* конечная точка, точно бот или негодяй */
                    return false;

                } else {
                    /* обновим количество запросов */
                    $obIp = SecIp::findOne(["ID" => $row->ID]);
                    $obIp->CONN_PER_TIME = ++$row->CONN_PER_TIME;
                    $obIp->update();

                    return true;
                }
            } else {
                /* обновим TIME_POINT и сбросим CONN_PER_TIME */
                $obIp = SecIp::findOne(["ID" => $row->ID]);
                $obIp->TIME_POINT = $time;
                $obIp->CONN_PER_TIME = 1;
                $obIp->update();

                return true;
            }
        } else {

            /* Запишем новую запись о подключении в таблицу */
            $obIp = new SecIp();
            $obIp->REAL_IP       = $this->ipData['REAL_IP'];
            $obIp->INT_IP        = (int)$this->ipData['INT_IP'];
            $obIp->USER_AGENT    = $this->getUserAgent();
            $obIp->SESSION_ID    = $this->session_id;
            $obIp->TIME_POINT    = time();
            $obIp->CONN_PER_TIME = 1;

            return $obIp->insert();
        }
    }

    private function checkCookie()
    {
        $cookie = \Yii::$app->request->cookies;
        return  $cookie->getValue($this->sessionName) && !empty($cookie->getValue($this->sessionName));
    }

    private function writeToBlackList($insertData)
    {
        $ob = new SecIpBlackList();
        $ob->INT_IP = $insertData['INT_IP'];
        $ob->IP_ID = $insertData['IP_ID'];
        $ob->insert();
    }

    /**
     * @param $INT_IP
     *
     * @return bool
     */
    private function maxConnectFromSingleIP($INT_IP)
    {
        $q1 = new Query();

        $q1->select('si.INT_IP, si.SESSION_ID, si.ID, SUM(si.CONN_PER_TIME) AS SUMM_CONN')
            ->from('sec_ip si')
            ->where("FROM_UNIXTIME(si.TIME_POINT + ( SELECT so.VALUE FROM sec_option so WHERE so.NAME = 'MAX_TIME_REQUEST_PER_IP')) > NOW()")
            ->andWhere("si.INT_IP = $INT_IP")
            ->groupBy('INT_IP');

        $res = $q1->one();

        if (is_array($res) && (int)$res['SUMM_CONN'] >= $this->option['MAX_REQUEST_PER_TIME_FROM_IP']) {
            $this->writeToBlackList([
                "IP_ID" => $res['ID'],
                "INT_IP" => $res['INT_IP']
            ]);
        }

        return !((int)$res['SUMM_CONN'] >= $this->option['MAX_REQUEST_PER_TIME_FROM_IP']);
    }

    private function inWhiteList()
    {
        $res = SecWhiteList::findOne(['INT_IP' => $this->ipData['INT_IP']]);

        if (!$res) return false;

        return ((int)$res->INT_IP === $this->ipData['INT_IP']);
    }
}