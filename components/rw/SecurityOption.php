<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 11.07.2017
 * Time: 17:22
 */

namespace app\components\rw;


use app\models\wiki\SecOption;
//use app\components\rw\SecCache;

class SecurityOption
{
    /**
     * @param      $data
     * @param null $value
     *
     * @return bool
     */
    public static function setOption( $data, $value = null )
    {
        if (is_array($data) && isset($data['NAME']) && isset($data['VALUE'])) {
            $secOption = SecOption::findOne(['NAME' => $data['NAME']]);
            $secOption->NAME = $data['NAME'];
            $secOption->VALUE = (string)$data['VALUE'];
        }
        else if (is_string($data) && !$value){
            $secOption = SecOption::findOne(['NAME' => $data]);
            $secOption->NAME = $data;
            $secOption->VALUE = (string)$value;
        }
        else return false;

        return $secOption->update();

    }

    /**
     * @param null $optName
     *
     * @return array
     */
    public static function getOption( $optName = null )
    {
        $ar = [];
        //$cache = new SecCache();

        if ( strlen($optName) > 1 ) {

            //if ($res = $cache->get(__CLASS__ . __METHOD__ . $optName))
                //return $res;

            $res = SecOption::findOne(['NAME' => $optName]);

            $ar[ $res->NAME ] = (int)$res->VALUE ?: $res->VALUE; // integer or string value

            //$cache->set(md5(__CLASS__ . __METHOD__ . $optName), $ar, 60*60*24);
        } else {

           // if ($res = $cache->get(__CLASS__ . __METHOD__ . "all"))
               // return $res;

            $res = SecOption::find()->all();

            foreach ($res as $row)
                $ar[ $row->NAME ] = (int)$row->VALUE ?: $row->VALUE; // integer or string value

            // $cache->set(md5(__CLASS__ . __METHOD__ . "all"), $ar, 60*60*24);
        }

        return $ar;
    }
}