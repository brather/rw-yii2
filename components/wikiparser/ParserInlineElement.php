<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 14.07.2017
 * Time: 19:18
 */

namespace app\components\wikiparser;

class ParserInlineElement {
    public $startTag, $endTag;
    public $argSep, $argNameSep;
    public $hasArgs;

    function ParserInlineElement($startTag, $endTag, $argSep = '', $argNameSep = '', $argLimit = 0) {
        $this -> startTag = $startTag;
        $this -> endTag = $endTag;
        $this -> argSep = $argSep;
        $this -> argNameSep = $argNameSep;
        $this -> argLimit = $argLimit;
        $this -> hasArgs = $this -> argSep != '';
    }
}