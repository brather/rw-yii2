<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 14.07.2017
 * Time: 19:17
 */
namespace app\components\wikiparser;

class ParserLineBlockElement {
    public $startChar;	/* Characters which can loop to start this element */
    public $endChar;	/* End character */
    public $limit;		/* Max depth of the element */
    public $nestTags;	/* True if the tags for this element need to made hierachical for nesting */

    function ParserLineBlockElement($startChar, $endChar, $limit = 0, $nestTags = true) {
        $this -> startChar = $startChar;
        $this -> endChar = $endChar;
        $this -> limit = $limit;
        $this -> nestTags = $nestTags;
    }
}