<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 14.07.2017
 * Time: 19:16
 */
namespace app\components\wikiparser;

class ParserTableElement {
    public $lineStart;	/* Token appearing at start of line */
    public $argsep;
    public $limit;
    public $inlinesep;

    function ParserTableElement($lineStart, $argsep, $inlinesep, $limit) {
        $this -> lineStart = $lineStart;
        $this -> argsep = $argsep;
        $this -> inlinesep = $inlinesep;
        $this -> limit = $limit;
    }
}