<?php

return [
    'adminEmail' => 'achechel@elar.com',
    'rabbitmq-dev' => [
        'host' => '185.98.83.176',
        'port' => 5672,
        'login' => 'admin',
        'password' => 'admin',
        'vhost' => 'eto_prod'
    ],
    'rabbitmq-prod' => [
        'host' => '172.16.62.12',
        'port' => 5672,
        'login' => 'admin',
        'password' => 'admin',
        'vhost' => 'eto_prod'
    ]
];
