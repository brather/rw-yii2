<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 09.06.2017
 * Time: 13:05
 */

$params = require(__DIR__ . '/params.php');

$db = YII_ENV_DEV ? require(__DIR__ . '/db.php') : require (__DIR__ . '/db_prod.php');


$config = [
    'id' => 'wiki',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true, //** Пользователь может войти без подтверждения */
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin'],
            'controllerMap' => [
                'security' => [
                    'class' => 'dektrium\user\controllers\SecurityController',
                    'layout' => '//page'
                ],
                'profile' => [
                    'class' => 'dektrium\user\controllers\ProfileController',
                    'layout' => '//page'
                ],
                // Контроллеру Registration переопределим layout
                'registration' => [
                    'class' => 'dektrium\user\controllers\RegistrationController',
                    'layout' => '//page'
                ]
            ]
        ]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'eW87_VVWJqg0c4fGE3dopHvHlohbgori',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            // Класс идентификатор модуля user
            'identityClass' => 'dektrium\user\models\User',
        ],
        'errorHandler' => [
            'errorAction' => 'wiki/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'showScriptName'=>false,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'rules' => [
                [
                    'pattern'   => 'search/<query>/<page:\d+>/<limit:\d+>',
                    'route'     => 'search/index',
                    'suffix'    => '/'
                ],
                [
                    'pattern'   => 'search/<query>/<page:\d+>',
                    'route'     => 'search/index',
                    'suffix'    => '/'
                ],
                '<controller:wiki>/Файл:<filename>'                                 => '<controller>/image',
                '<controller:page>/<code:[\d?\w-_]+>'                               => '<controller>/index',
                '<controller:page>/a:<action>'                               => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w\d-_/]>'                            => '<controller>/<action>',
                '<controller:[\w-]+>/<suffix:[\w_]+>:<title>/<category:[\w\d_-]+>'  => '<controller>/page',
                '<controller:[\w-]+>/<suffix:[\w_]+>:<title>'                       => '<controller>/page',
                '<controller:search>/<query>'                                       => '<controller>/index',
                '<controller:[\w-]+>/'                                              => '<controller>/index',
            ],
        ],
    ],
    'params' => $params,
    'defaultRoute' => 'wiki/index',
    'language' => 'ru',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '10.4.9.34'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '10.4.9.34'],
    ];
    /* INot cached on DEV */
    unset($config['components']['cache']);
}

return $config;
