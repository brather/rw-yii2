<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 04.07.2017
 * Time: 9:30
 */

namespace app\controllers;

use app\components\jungle\Jungle_WikiSyntax_Parser;
use app\components\mwparser\Parser;
use app\components\mwparser\ParserOptions;
use app\components\wikiparser\WikitextParser;
use \app\models\wiki\StaticPages;
use yii\web\HttpException;


class PageController extends \yii\web\Controller
{
    public $layout = 'page';

    /* No default page, so redirect on main */
    public function actionIndex( $code ) {

        $db = StaticPages::findOne(['alias' => $code, 'active' => 1]);
        if ($db === null)
            throw new HttpException(404);
        else
            $res = $db->toArray();

        $arRes = [
            'meta' => [
                'title' => @$res['meta_title'],
                'description' => @$res['meta_description'],
                'keywords' => @$res['meta_keywords']
            ],
            'title' => $res['title'],
            'last_update' => $res['last_update'],
            'create_date' => $res['create_date'],
            'alias' => $res['alias'],
            'id' => $res['id'],
            'text' => $res['text']
        ];

        return $this->render('@app/views/wiki/static_page', $arRes);
    }

    public function actionParse() {

        $content = file_get_contents("../fileparse.txt");

        $parseText = new Jungle_WikiSyntax_Parser($content, "Some Title");

        $res['parseText'] = $parseText->clean_wiki_text($parseText->parse());


        return $this->render('@app/views/wiki/page_parse', $res);
    }

}