<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 05.07.2017
 * Time: 10:59
 */

namespace app\controllers;

use app\components\ElasticHelper;
use yii\data\Pagination;
use yii\web\Controller;

class SearchController extends Controller {

    public $layout = 'page';

    public function actionIndex($query, $page = 1, $limit = 10)
    {
        $rq = \Yii::$app->getRequest();
        /* null OR number of page */
        $pageNumber = $rq->get('page') ? : 1;

        /* Limit default 10 */
        $limit = $rq->get('limit') ? : 10;
        $offset = ($pageNumber > 1) ? $limit * $pageNumber : 0;

        $params = [
            'q' => $query,
            'limit' => (string)$limit,
            'offset' => (string)$offset
        ];

        $res["RESULT"] = ElasticHelper::init([
            'url' => 'fts',
            'params' => $params
        ])->run();

        $pages = new Pagination([
            'totalCount' => $res['RESULT']['total'],
            'pageSize' => $limit,
            'defaultPageSize' => $limit,
            'forcePageParam' => false
        ]);
        $pages->pageSizeParam = 'limit';
        $pages->route = 'search/index';

        $res['PAGES'] = $pages;

        return $this->render('@app/views/wiki/search', $res);
    }
}