<?php

namespace app\controllers;

use app\assets\ErrorAsset;
use app\assets\MainAsset;
use app\assets\MainSourceAsset;
use app\assets\PageAsset;
use app\components\ElasticHelper;
use app\components\rw\Security;
use Yii;
use yii\web\Application;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class WikiController extends \yii\web\Controller
{
    public $layout = 'wiki_index';

    public function actionIndex()
    {
        $rq = Application::getInstance()->getRequest();
        $scriptFile = $rq->getScriptFile();
        $baseUrl = $rq->getBaseUrl();
        $url = $rq->getUrl();
        /* Local redirect */
        if ('/wiki/' !== $url ) $this->redirect("/wiki/", 301);

        Yii::setAlias("@webroot", dirname($scriptFile));
        Yii::setAlias("@wiki", $baseUrl);

        /* Asset для главной страницы */
        MainAsset::register($this->view);

        return $this->render('index');
    }

    public function actionImage ($filename)
    {
        $imgPath = $_SERVER['DOCUMENT_ROOT'] . "/images/" . substr(md5($filename), 0, 1) . "/" . substr(md5($filename), 0, 2) . "/" . $filename;

        if (!file_exists($imgPath))
            $imgPath = $_SERVER['DOCUMENT_ROOT'] . "/images/thumb/" . substr(md5($filename), 0, 1) . "/" . substr(md5($filename), 0, 2) . "/" . $filename;

        $fp = fopen($imgPath, "rb");
        $imageInfo = getimagesize($imgPath);

        header('Content-Type: ' . $imageInfo['mime']);
        header('Content-Length: ' . filesize($imgPath));

        fpassthru($fp);
        die();
    }

    public function actionPage( $suffix, $title, $category = "" )
    {
        /* First procedure - IMPORTANT! */
        Security::onBeforePageDisplay();

        /* параметры согласно развернутой версии prod или dev - @app/config/params.php */
        $mqParam = \Yii::$app->params['rabbitmq-' . YII_ENV];

        $mqConn = new AMQPStreamConnection($mqParam['host'], $mqParam['port'], $mqParam['login'], $mqParam['password'], $mqParam['vhost']);
        $mqJSON = [
            "page_stat" => [
                "session_id" => session_id(),
                "remote_ip_addr" => isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $_SERVER['REMOTE_ADDR'],
                "full_title" => $suffix . ":" . $title . "/" . $category,
                "title" => $title,
                "source" => $category,
                "namespace" => $suffix,
                "request_datetime" => date("Y-m-d H:i:s")
            ]
        ];

        $rq = Application::getInstance()->getRequest();

        $mqJSON['page_stat']['url'] = $rq->getUrl();

        $q = $rq->get('q') ? : $rq->get('query');

        if (!empty($q))
            $mqJSON['page_stat']['q'] = $q;

        $baseUrl = $rq->getBaseUrl();
        Yii::setAlias("@wiki", $baseUrl);

        /* Asset для страницы статьи */
        MainSourceAsset::register($this->view);

        $arRes = [
            'RANDOM_PAGES' => [], 'SIMILAR_CONTENT' => [], 'PAGE_CONTENT' => [], 'OTHER_CONTENT' => []
        ];

        $arRes['PAGE_CONTENT'] = ElasticHelper::init(['url' => 'page'])->run();

        $mqJSON['page_stat']['page_id'] = $arRes['PAGE_CONTENT']['page_id'];
        $mqJSON['page_stat']['page_category'] = $arRes['PAGE_CONTENT']['page_category'];

        $mqChannel = $mqConn->channel();

        $mqChannel->queue_declare('page_stat');
        $mqChannel->exchange_declare('php.page.stat', 'fanout');
        $mqChannel->queue_bind('page_stat', 'php.page.stat');

        if (!empty($q)) {
            $mqChannel->queue_declare('search_query_stat');
            $mqChannel->exchange_declare('search.query.stat', 'fanout');
            $mqChannel->queue_bind('search_query_stat', 'search.query.stat');
            $mqQMsg = new AMQPMessage(json_encode(['search_query_stat' => [
                'query' => $q,
                'request_datetime' => date("Y-m-d H:i:s"),
                'session_id' => session_id(),
            ]], JSON_UNESCAPED_UNICODE));

            $mqChannel->basic_publish($mqQMsg, 'search.query.stat');
        }

        $mqMsg = new AMQPMessage(json_encode($mqJSON, JSON_UNESCAPED_UNICODE));
        $mqChannel->basic_publish($mqMsg, 'php.page.stat');

        $mqChannel->close();
        $mqConn->close();

        if (isset($arRes['PAGE_CONTENT']['page_main_name']) && isset($arRes['PAGE_CONTENT']['page_main_name'])) {
            $arRes['SIMILAR_CONTENT'] = ElasticHelper::init([
                'url' => 'search',
                'params' => [
                    'q' => $arRes['PAGE_CONTENT']['page_main_name'],
                    'source_id_out' => $arRes['PAGE_CONTENT']['page_source_short_name'],
                    'exact' => '1',
                    'limit' => '3',
                    'preview' => '150'
                ]
            ])->run();

            if ($rq->getIsGet() && !empty($rq->get('q'))) {
                $arRes['OTHER_CONTENT'] = ElasticHelper::init([
                    'url' => 'search',
                    'params' => [
                        'q' => $rq->get('q'), // то что пришло посиковым запросом
                        'not_title' => $arRes['PAGE_CONTENT']['page_main_name'], // чистый title статьи
                        'limit' => '3',
                        'preview' => '150'
                    ]
                ])->run();
            }
        }

        $arRes['RANDOM_PAGES'] = ElasticHelper::init(['url' => 'random'])->run();


        return $this->render('page', $arRes);
    }

    public function actionError() {

        /* Asset для главной страницы */
        ErrorAsset::register($this->view);

        return $this->render('@app/views/wiki/error');
    }

}
