$(function(){
	$(document).on('click','[data-active-toggle]',function(){//тоглит свой актив
		$(this).toggleClass('active');
	});
    $(document).on('click', '[data-active-cca-toggle], .mw-collapsible-toggle', function (event) {
        var toggler = $(this);
        // comfortWait = setTimeout(delayedDisplay, 100);
        if ( toggler.hasClass('active') ) {
            toggler.removeClass('active cca-pre cca-ready')
        } else {
            delayedDisplay()
        }
        function delayedDisplay() {
            // toggler.parent().siblings().removeClass('open cca-pre cca-ready');
            toggler.toggleClass('active', true);
            if (window.requestAnimationFrame) {
                toggler.toggleClass('cca-pre', true);
                requestAnimationFrame(changeClassAnimationPre);
                
            }
        }
        function changeClassAnimationPre() {
            // toggler.toggleClass('cca-pre', true);
            requestAnimationFrame(changeClassAnimationReady);
        }
        function changeClassAnimationReady() {
            toggler.toggleClass('cca-ready', true);
        }
    });

    var getRelevanceObject = {//Настройки для виджета автокомплита
        _create: function () {
            this._super();  // UI-мантра
            this.widget().menu('option', 'items', '> :not(.ui-autocomplete-category)'); // настройка опции пунктов меню, Default: "> *"
        },
        _makeHref: function(items) { // метод для вызова в _renderMenu, генерирует ссылку для отправки по нажатию на "искать" при вводе с точным совпадением с одним из вариантов
            var sInput = $(this.element),
                typed = sInput.val(),
                tFiltered = typed.match(/^\s*(.+[^\s])\s*$/);
            sInput.removeAttr('data-href');
            // console.log(tFiltered);
            // console.log(tFiltered[1]);
            
            // $.each(items, function (index, item) {                
            //     if ( tFiltered !== null && tFiltered.length > 1 && item.title_raw.toLowerCase() === tFiltered[1].toLowerCase()) {
            //         //sInput.attr('data-href', items[0].link);
            //         sInput.attr('data-href', item.link + "?q=" + typed);
            //         return null;
            //     }
            // });

            if (items.length && items[0].category != 'fuzzy') {
                sInput.attr('data-href', items[0].link + "?q=" + typed);
            }
        },
        _renderMenu: function (ul, items) {//Пилить категории тут
            var that = this,
                currentCategory = '';
            $.each(items, function (index, item) {
                var li,itemcat;
                if (item.category !== currentCategory) {
                    ul.append('<li class="ui-autocomplete-category">возможно вы имели в виду</li>');
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);
            });
            this._makeHref(items);
            //$('<li class="li-fulltext" aria-label="li-fulltext">').append("<div class='suggestion '><a href='/index.php?title=Служебная:Поиск&search=" + searchstring_value + "&fulltext=Search'>содержащие...<br><em>" + searchstring_value +"</em></a></div>").appendTo(ul);
        },
        _renderItem: function (ul, item) {
            var li = $('<li>'),
                link = $('<a href="' + item.link + '">' + item.label + ((item.term !== undefined) ? '<span class="link">' + item.term + '</span>' : '') + '</a>');
            $(link).on('click',function(e){ e.preventDefault() });
            li.append(link);
            li.appendTo(ul);
            return li;
        },
        _resizeMenu: function () { // width fix in a case of long suggestion text
            var ul = this.menu.element;
            ul.outerWidth(this.element.outerWidth());
        }
    };

    /* Press btn search */
    $('button.btn.search-button').on('click', function () {
        var sInput = $('#search_field');
        if (sInput.data('href')) {
            document.location.href = sInput.data('href');
        } else if (sInput.val() !== "") {
            document.location.href = '/search/' + sInput.val().match(/^\s*(.+[^\s])\s*$/)[1].toLowerCase()
        }
    });

    $.fn.livesearch = function(options) {
        var settings = $.extend({
            submit: true
        }, options );
        this.each(function() {
            var sInput = $(this);
            if (settings.submit) {
                $(this).keydown(function(event){
                    $("#searchclear").toggle(Boolean($(this).val()));
                    if(event.keyCode === 13 ||  event.keyCode === 108){
                        // event.preventDefault();
                        //$(this).closest('form').submit();
                        if (sInput.data('href')) {
                            // console.log('get article ', sInput.data('href'));
                            document.location.href = sInput.data('href');
                        } else {
                            // console.log('submit search');
                            document.location.href = '/search/' + sInput.val().match(/^\s*(.+[^\s])\s*$/)[1].toLowerCase()
                        }
                    }
                });
            }
            $.widget('custom.getrelevance', $.ui.autocomplete, getRelevanceObject);
            sInput.getrelevance({
                delay: 100,
                minLength: 3,
                source: getSearchData,
                select: function(event,ui) {
                    event.preventDefault(); //отменяем заполнение поля из меню, заполяем сами
                    $(this).val(ui.item.title_raw);
                    $(this).attr('data-href',ui.item.link + '?q=' + ui.item.title_raw);
                },
                focus: function(event,ui) {
                    event.preventDefault(); //отменяем заполнение поля из меню, заполяем сами
                    $(this).val(ui.item.title_raw);
                    $(this).attr('data-href',ui.item.link + '?q=' + ui.item.title_raw);
                },
                autofocus: true
            });
            function getSearchData(request, response){
                var suggestions = [];
                $('#search_field').removeAttr('data-href');
                $.ajax({
                    url: "http://185.98.83.176/api/titles/autocomplete",
                    dataType: 'json',
                    crossDomain: true,
                    data: {
                        q: sInput.val()
                    },
                    success: function(data){
                        if (data.hasOwnProperty('titles_exact')) {
                            for (var i in data.titles_exact) {
                                suggestions.push({
                                    label: data.titles_exact[i].title,
                                    category: "",
                                    link: data.titles_exact[i].link,
                                    title_raw: data.titles_exact[i].title_raw
                                })
                            }
                        }
                        if (data.hasOwnProperty('titles_fuzzy')) {
                            for (i in data.titles_fuzzy) {
                                suggestions.push({
                                    label: data.titles_fuzzy[i].title,
                                    category: "fuzzy",
                                    link: data.titles_fuzzy[i].link,
                                    title_raw: data.titles_fuzzy[i].title_raw
                                })
                            }
                        }

                        // if (suggestions.length === 0) {
                        //     suggestions.push({label: "нет результатов поиска: ", category: "", term: sInput.val()})
                        // }
                        response(suggestions);
                    }
                });
            }
        });
    };

   //      $("#search_field").rw_autocomplete({
   //          source: [],
			// appendTo: "#search_field_container",
   //          select: function (event, ui) {
   //              if (typeof ui.item === "object" && ui.item.title_raw !== undefined)
   //                  ui.item.value = ui.item.title_raw;
   //              else {
   //                  return false;
   //              }
   //          },
   //          focus: function (event, ui) {
   //              if (typeof ui.item === "object" && ui.item.title_raw !== undefined) {
   //                  ui.item.value = ui.item.title_raw;
			// } else {
   //                  return false;
   //              }
   //          }
   //      });

    $('[data-search-field]').livesearch();

    $('#search_field').on('submit', function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (moveToLink = $(this).attr('data-href'))
            window.location = moveToLink + "?searchTitle=" + $('#searchstring').val() + "#first_article";

        return false;
    });

    $('.personal_filters-controls-buttons button').on("click", function() {
        $("header.personal_filters-controls").hide();
    });

    if (window.location.pathname === "/wiki/") {
        $(window).scroll(function () {
            if (!isLoadingArticles && $(window).scrollTop() >= $(document).height() - $(window).height() - 30) {
                isLoadingArticles = true;
                get_articles();
            }
        });
    }
    /*
    function get_articles() {
        $.ajax({
            url: "http://185.98.83.176/api/titles/random",
            type: "GET",
            dataType: "json"
        }).done(function(data) {
            $("#articles_container").append( $("#articles_tmpl").tmpl(data) );
        });
    }*/

    var	isLoadingArticles = true,
			offset = 0,
			ar10 = new Array(10),
			//seed = ar10.join((Math.random().toString(36)+'00000000000000000').slice(2, 18)).slice(0, 9); // absolutely random seed
			now = new Date();
	var	seed = now.getFullYear().toString() + now.getMonth().toString() + now.getDate().toString() + now.getHours().toString();
			

    function get_articles() {
        var n = 8; // articles per time
        $("#articles_container").append('<p align="center" id="loadingIndicator">Загружаем ещё статьи</p>');
        $.ajax({
            url: "http://185.98.83.176/api/titles/random",
            type: "GET",
            dataType: "json",
            data: { illustrated: 1, limit: n, offset: offset, seed: seed }
        }).done(function(data) {
            $("#loadingIndicator").remove();
            $("#articles_container").append( $("#articles_tmpl").tmpl(data) );
            offset += n;
            isLoadingArticles = false;
        });
    }

    if (window.location.pathname === "/wiki/") get_articles();

    $("#searchclear")
        .toggle(Boolean($("#search_field").val()))
        .click(function(){
            $("#search_field").val('').focus().removeAttr('data-href');
            $(this).hide();
        });
});