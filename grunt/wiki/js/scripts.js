var FRONT = FRONT || {};
FRONT.Utils = {
    addClass: function addClass(obj, cls) {
        var classes = obj.className ? obj.className.split(' ') : [];

        for (var i = 0; i < classes.length; i++) {
            if (classes[i] == cls) return; // класс уже есть
        }

        classes.push(cls); // добавить

        obj.className = classes.join(' '); // и обновить свойство
    },
    removeClass: function removeClass(obj, cls) {
        var classes = obj.className.split(' ');

        for (var i = 0; i < classes.length; i++) {
            if (classes[i] == cls) {
                classes.splice(i, 1); // удалить класс
                i--; // (*)
            }
        }
        obj.className = classes.join(' ');

    },
    toggleClass: function toggleClass(element, className){
        if (!element || !className){
            return;
        }
        var classString = element.className,
            nameIndex = classString.indexOf(className);
        if (nameIndex == -1) {
            classString += ' ' + className;
        } else {
            classString = classString.substr(0, nameIndex) + classString.substr(nameIndex+className.length);
        }
        element.className = classString;
    }
};

(function(){
    // уменьшение меню
    var nav = document.getElementById('pageHeader');
    window.onscroll = function() {
        if ( window.pageYOffset > 45 ) {
            nav.className = "page_header shrinked";
        } else {
            nav.className = "page_header";
        }
    };

    // сайдбар
    var togglers = document.querySelectorAll('[data-aside-toggler]');

    [].forEach.call(togglers, function(elem) {
        elem.addEventListener('click', function(e){
            // console.log('fire');
            //document.body.classList.toggle('aside-turned-on')
            FRONT.Utils.toggleClass(document.body,'aside-turned-on')
        });
    });
})();


