<?php

use yii\db\Migration;

class m170718_093259_geo_object extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170718_093259_geo_object cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable($this->getDb()->tablePrefix . 'geo_object',
            [
                'page_id' => $this->integer(),
                'place_id' => $this->integer(),
                'lat' => $this->double(),
                'lon' => $this->double(),
                'display_name' => $this->string(),
                'class' => $this->string(),
                'type' => $this->string(),
                'importance' => $this->double()
            ]);
        $this->createIndex(
            'page_id-type-class-importance',
            $this->getDb()->tablePrefix . 'geo_object',
            ['page_id', 'type', 'class', 'importance']);
    }

    public function down()
    {
        $this->dropTable($this->getDb()->tablePrefix . 'geo_object');
    }
}
