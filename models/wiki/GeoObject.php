<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "{{%geo_object}}".
 *
 * @property integer $page_id
 * @property integer $place_id
 * @property double $lat
 * @property double $lon
 * @property string $display_name
 * @property string $class
 * @property string $type
 * @property double $importance
 */
class GeoObject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%geo_object}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'place_id'], 'integer'],
            [['lat', 'lon', 'importance'], 'number'],
            [['display_name', 'class', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'place_id' => 'Place ID',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'display_name' => 'Display Name',
            'class' => 'Class',
            'type' => 'Type',
            'importance' => 'Importance',
        ];
    }

    /**
     * @inheritdoc
     * @return GeoObjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GeoObjectQuery(get_called_class());
    }
}
