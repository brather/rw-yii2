<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $page_id
 * @property integer $page_namespace
 * @property string $page_title
 * @property string $page_restrictions
 * @property integer $page_is_redirect
 * @property integer $page_is_new
 * @property double $page_random
 * @property string $page_touched
 * @property resource $page_links_updated
 * @property integer $page_latest
 * @property integer $page_len
 * @property resource $page_content_model
 * @property resource $page_lang
 * @property integer $load_source_id
 * @property string $page_main_name
 * @property string $page_rd_title
 * @property double $page_rating
 * @property integer $page_category
 * @property string $page_title_origin
 * @property string $termin_type
 * @property string $page_title_new
 * @property string $page_title_fio
 * @property string $page_title_fio_2
 * @property string $page_title_fio_3
 * @property integer $pu_id
 * @property string $title_image
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_namespace', 'page_title', 'page_restrictions', 'page_random', 'page_latest', 'page_len'], 'required'],
            [['page_namespace', 'page_is_redirect', 'page_is_new', 'page_latest', 'page_len', 'load_source_id', 'page_category', 'pu_id'], 'integer'],
            [['page_restrictions'], 'string'],
            [['page_random', 'page_rating'], 'number'],
            [['page_title', 'page_main_name', 'page_rd_title', 'page_title_origin', 'termin_type', 'page_title_new', 'page_title_fio', 'page_title_fio_2', 'page_title_fio_3'], 'string', 'max' => 255],
            [['page_touched', 'page_links_updated'], 'string', 'max' => 14],
            [['page_content_model'], 'string', 'max' => 32],
            [['page_lang'], 'string', 'max' => 35],
            [['title_image'], 'string', 'max' => 1000],
            [['page_namespace', 'page_title'], 'unique', 'targetAttribute' => ['page_namespace', 'page_title'], 'message' => 'The combination of Page Namespace and Page Title has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'page_namespace' => 'Page Namespace',
            'page_title' => 'Page Title',
            'page_restrictions' => 'Page Restrictions',
            'page_is_redirect' => 'Page Is Redirect',
            'page_is_new' => 'Page Is New',
            'page_random' => 'Page Random',
            'page_touched' => 'Page Touched',
            'page_links_updated' => 'Page Links Updated',
            'page_latest' => 'Page Latest',
            'page_len' => 'Page Len',
            'page_content_model' => 'Page Content Model',
            'page_lang' => 'Page Lang',
            'load_source_id' => 'Load Source ID',
            'page_main_name' => 'Для синонимов',
            'page_rd_title' => 'Page Rd Title',
            'page_rating' => 'рейтинг стратьи для поиска (больше - лучше)',
            'page_category' => 'категория статьи для поиска',
            'page_title_origin' => 'Название статьи до нормализации',
            'termin_type' => 'Вид описываемого объекта (появилось после нормализации)',
            'page_title_new' => 'Новое название',
            'page_title_fio' => 'Новое название для фамилий',
            'page_title_fio_2' => 'Новое название для фамилий Передача 2',
            'page_title_fio_3' => 'Новое название для фамилий Передача 3',
            'pu_id' => 'Идентификатор статьи в базе ПУ',
            'title_image' => 'Title Image',
        ];
    }

    /**
     * @inheritdoc
     * @return PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }
}
