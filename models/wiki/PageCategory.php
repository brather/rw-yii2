<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "page_category".
 *
 * @property integer $page_category_id
 * @property string $page_category_name
 * @property double $page_category_rating
 */
class PageCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_category_name'], 'required'],
            [['page_category_rating'], 'number'],
            [['page_category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_category_id' => 'Page Category ID',
            'page_category_name' => 'Page Category Name',
            'page_category_rating' => 'Page Category Rating',
        ];
    }

    /**
     * @inheritdoc
     * @return PageCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageCategoryQuery(get_called_class());
    }
}
