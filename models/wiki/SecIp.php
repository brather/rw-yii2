<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "sec_ip".
 *
 * @property integer $ID
 * @property string $REAL_IP
 * @property integer $INT_IP
 * @property string $USER_AGENT
 * @property string $SESSION_ID
 * @property integer $TIME_POINT
 * @property integer $CONN_PER_TIME
 */
class SecIp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sec_ip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['REAL_IP', 'INT_IP', 'TIME_POINT', 'CONN_PER_TIME'], 'required'],
            [['INT_IP', 'TIME_POINT', 'CONN_PER_TIME'], 'integer'],
            [['REAL_IP'], 'string', 'max' => 15],
            [['USER_AGENT'], 'string', 'max' => 200],
            [['SESSION_ID'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'REAL_IP' => 'Real  Ip',
            'INT_IP' => 'Int  Ip',
            'USER_AGENT' => 'User  Agent',
            'SESSION_ID' => 'Session  ID',
            'TIME_POINT' => 'Time  Point',
            'CONN_PER_TIME' => 'Conn  Per  Time',
        ];
    }

    /**
     * @inheritdoc
     * @return SecIpQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SecIpQuery(get_called_class());
    }
}
