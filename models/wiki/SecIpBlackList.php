<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "sec_ip_black_list".
 *
 * @property integer $ID
 * @property integer $IP_ID
 * @property string $CREATE_DATE
 * @property integer $INT_IP
 * @property string $HOST
 * @property string $COUNTRY_ID
 * @property integer $BLACK_LIST_ID
 */
class SecIpBlackList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sec_ip_black_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IP_ID', 'INT_IP', 'BLACK_LIST_ID'], 'integer'],
            [['CREATE_DATE'], 'safe'],
            [['INT_IP'], 'required'],
            [['HOST'], 'string', 'max' => 255],
            [['COUNTRY_ID'], 'string', 'max' => 5],
            [['INT_IP'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'IP_ID' => 'Ip  ID',
            'CREATE_DATE' => 'Create  Date',
            'INT_IP' => 'Int  Ip',
            'HOST' => 'Host',
            'COUNTRY_ID' => 'Country  ID',
            'BLACK_LIST_ID' => 'from https://myip.ms/browse/scam',
        ];
    }

    /**
     * @inheritdoc
     * @return SecIpBlackListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SecIpBlackListQuery(get_called_class());
    }
}
