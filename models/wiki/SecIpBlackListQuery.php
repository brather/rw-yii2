<?php

namespace app\models\wiki;

/**
 * This is the ActiveQuery class for [[SecIpBlackList]].
 *
 * @see SecIpBlackList
 */
class SecIpBlackListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SecIpBlackList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SecIpBlackList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
