<?php

namespace app\models\wiki;

/**
 * This is the ActiveQuery class for [[SecIp]].
 *
 * @see SecIp
 */
class SecIpQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SecIp[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SecIp|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
