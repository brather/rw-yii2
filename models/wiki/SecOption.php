<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "sec_option".
 *
 * @property string $NAME
 * @property string $VALUE
 */
class SecOption extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sec_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'VALUE'], 'required'],
            [['NAME'], 'string', 'max' => 50],
            [['VALUE'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NAME' => 'Name',
            'VALUE' => 'Value',
        ];
    }

    /**
     * @inheritdoc
     * @return SecOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SecOptionQuery(get_called_class());
    }

}
