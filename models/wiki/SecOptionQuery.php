<?php

namespace app\models\wiki;

/**
 * This is the ActiveQuery class for [[SecOption]].
 *
 * @see SecOption
 */
class SecOptionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SecOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SecOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
