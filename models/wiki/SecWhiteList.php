<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "sec_white_list".
 *
 * @property integer $INT_IP
 * @property string $REAL_IP
 */
class SecWhiteList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sec_white_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['INT_IP', 'REAL_IP'], 'required'],
            [['INT_IP'], 'integer'],
            [['REAL_IP'], 'string', 'max' => 15],
            [['INT_IP', 'REAL_IP'], 'unique', 'targetAttribute' => ['INT_IP', 'REAL_IP'], 'message' => 'The combination of Int  Ip and Real  Ip has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INT_IP' => 'Int  Ip',
            'REAL_IP' => 'Real  Ip',
        ];
    }

    /**
     * @inheritdoc
     * @return SecWhiteListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SecWhiteListQuery(get_called_class());
    }
}
