<?php

namespace app\models\wiki;

/**
 * This is the ActiveQuery class for [[SecWhiteList]].
 *
 * @see SecWhiteList
 */
class SecWhiteListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SecWhiteList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SecWhiteList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
