<?php

namespace app\models\wiki;

use Yii;

/**
 * This is the model class for table "{{%rw_static_pages}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $text
 * @property string $create_date
 * @property string $last_update
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $active
 * @property string $comment
 */
class StaticPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%static_pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['text', 'comment'], 'string'],
            [['create_date', 'last_update'], 'safe'],
            [['active'], 'integer'],
            [['alias'], 'string', 'max' => 50],
            [['title', 'meta_description', 'meta_keywords'], 'string', 'max' => 500],
            [['meta_title'], 'string', 'max' => 200],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'text' => 'Text',
            'create_date' => 'Create Date',
            'last_update' => 'Last Update',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'active' => 'Active',
            'comment' => 'Comment',
        ];
    }
}
