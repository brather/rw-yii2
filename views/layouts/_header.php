<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 04.07.2017
 * Time: 12:32
 */

use yii\helpers\Url;

$rq = \Yii::$app->getRequest();

$url = $rq->getUrl();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <title><?= $this->title ? $this->title . " - " : "" ; ?>Энциклопедия терминов и определений</title>
    <meta type="keywords" content="<?= @$this->metaTags['keywords']?>" />
    <meta type="description" content="<?= @$this->metaTags['description']?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <?php $this->head() ?>
</head>
<body>
<header class="page_header" id="pageHeader">
    <div class="page_header-wrapper">
        <div class="page_header-logo">
            <figure>
                <picture>
                    <a href="/wiki/" title="Энциклопедия терминов и определений"><img src="/front_images/logo2.png"/></a>
                </picture>
                <a href="/wiki/" title="Энциклопедия терминов и определений"><figcaption>Энциклопедия<br>
                        терминов и определений</figcaption></a>
            </figure>
        </div>
        <div class="page_header-search form-group">
            <div class="input-group" id="search_field_container">
                <input type="text" class="form-control" id="search_field" data-search-field value="<?= \Yii::$app->getRequest()->get('q') ? : \Yii::$app->getRequest()->get('query') ?>"/>
                <span id="searchclear" class="glyphicon glyphicon-remove-circle"></span>
                <span class="input-group-btn">
							<button class="btn btn-primary btn-block search-button"></button>
						</span>
            </div>
        </div>
		<div class="page_header-feedback">
				<button class="btn btn-warning" onClick="window.open('http://feedback.eto.global/create','_blank');">Написать отзыв</button>
		</div>
        <div class="page_header-menutoggler">
            <button class="btn btn-default page_header-menutoggler-button" id="asideToggler" data-aside-toggler></button>
        </div>
    </div>
</header>
<header class="personal_filters personal_filters-controls hidden">
    <fieldset>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Математика</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Русский &shy;язык</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Информа&shy;тика</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Физика</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Химия</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Биология</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">География</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">История</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Политика</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Астрономия</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Промышлен&shy;ность</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Архитектура</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Искусство</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Правове&shy;дение</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Анатомия</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Спорт</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Аналитика</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Геология</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Религия</span>
        </label>
        <label class="text-checkbox">
            <input type="checkbox" name="filters">
            <span class="lbl">Общество</span>
        </label>
    </fieldset>
    <div class="personal_filters-controls-buttons">
        <button type="submit" class="btn btn-primary">Применить</button>
        <button type="submit" class="btn btn-default">Выбрать позже</button>
    </div>
</header>

<aside class="aside_panel">
    <div class="aside_panel-wrapper">
        <nav class="aside_search">
            <div class="aside_panel-search_block" id="!search_field_container">
                <input type="text" class="form-control" id="!search_field" data-search-field/>
                <span>
                        	<button class="btn btn-default page_header-menutoggler-button" id="asideToggler" data-aside-toggler=""></button>
                    <!-- <button class="btn btn-primary btn-block search-button"></button> -->
						</span>
            </div>
        </nav>
        <!--<nav class="aside_panel-enter">
            <button class="btn btn-primary">Войти</button>
        </nav>-->
        <nav>
            <a href="<?= Url::to("/page/about") ?>">Об энциклопедии</a>
        </nav>
        <nav class="aside_panel-settings">
            <a href="http://feedback.eto.global/create" target="_blank">Обратная связь</a>
            <a href="<?= Url::to("/page/abuse");?>">Правообладателям</a>
        </nav>
    </div>
</aside>
<? if ($url === '/wiki/'): ?>
<header class="jumbo_notification bs-callout">
<div class="jumbotron">
<h1>Ресурс, удобный для&nbsp;всех</h1>
<p>Пожалуйста, напишите нам о&nbsp;любой ошибке или&nbsp;проблеме в&nbsp;работе энциклопедии. Нам важен каждый&nbsp;отзыв!</p>
<div class="personal_filters-controls-buttons">
        <a href="http://feedback.eto.global/create" target="_blank"><button type="submit" class="btn btn-primary">Написать отзыв</button></a>
</div>
</div>
</header>
<? endif; ?>
<section class="main_content">