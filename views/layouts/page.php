<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 09.06.2017
 * Time: 17:41
 */
use app\assets\PageAsset;

PageAsset::register($this);
?>
<?php $this->beginPage() ?>
    <? require_once "_header.php"; ?>

    <?=$content?>

    <? require_once "_footer.php"?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>