<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$code = \Yii::$app->response->getStatusCode();
$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <? if ($code === 405):?>
        <h1>Доступ заблокирован</h1>
        <p>В связи с большим числом обращений с вашего IP-адреса, доступ к ресурсу был ограничен. Если вы считаете, что это произошло по ошибке, свяжитесь с нами через форму <a href="http://dev.rosswiki.ru/create">обратной связи</a>.</p>
    <? else: ?>
        <h1>Ошибка 404</h1>
        <h2>Страница не найдена</h2>
        <p>Страница, к&nbsp;которой вы&nbsp;пытаетесь обратиться, не&nbsp;существует. Проверьте правильность указания адреса или&nbsp;перейдите на&nbsp;<a href="http://eto.global">главную&nbsp;страницу</a>.</p>
    <? endif;?>

</div>
