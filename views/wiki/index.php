<?php
/* @var $this yii\web\View */
?>
<script id="articles_tmpl" type="text/x-jquery-tmpl">
{{each(index, artcl) articles}}
	{{if typeof(artcl.picture) != 'undefined' && artcl.picture != null}}
		<article class="main_content-item-small bordered {{if artcl.picture_color == "dark"}}  {{/if}}">
			<a href="/wiki/${artcl.full_title}"><picture style="background-image: url('/${artcl.picture}')"> <img src="/${artcl.picture}"/> </picture></a>
			<h4><a href="/wiki/${artcl.full_title}">{{if artcl.title == artcl.title_short}}${artcl.title}{{else}}${artcl.title_short}{{/if}}</a></h4>
			<a href="/wiki/${artcl.full_title}"><p>${artcl.preview}</p></a>
		</article>

	{{else}}
		<article class="main_content-item-small bordered titleonly">
			<h4><a href="/wiki/${artcl.full_title}">${artcl.title}</a></h4>
			<div class="likes-view">&nbsp;</div>
		</article>
	{{/if}}
{{/each}}
</script>

<div id="articles_container"></div>
