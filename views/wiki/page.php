<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 03.07.2017
 * Time: 11:33
 */
use yii\helpers\Url;

?>
<header class="main_content-article_header">
    <? if (\Yii::$app->getRequest()->get('q')):?>
        <p class="bs-callout bs-callout-info">Открыта первая найденная статья. Перейти к <a href="<?= Url::to(["search/" . \Yii::$app->getRequest()->get('q')])?>">другим результатам поиска</a></p>
    <? endif; ?>
    <h1><?= $PAGE_CONTENT['page_main_name'] ?></h1>
</header>
<article class="main_content-article">
    <header>
        <h2><?= $PAGE_CONTENT['page_source_name'] ?></h2>
    </header>
    <div class="main_content-article_content">
        <?= $PAGE_CONTENT['text']; ?>
    </div>
    <aside class="main_content-article_aside_blocks">
        <? if (is_array($SIMILAR_CONTENT) && isset($SIMILAR_CONTENT['count']) && $SIMILAR_CONTENT['count'] > 0) { ?>
            <div class="main_content-aside_block">
                <h3>Смотреть в других источниках</h3>
                <? for ($i = 0; $i < 3 && $i < (int)$SIMILAR_CONTENT['count']; $i++): ?>
                    <div class="main_content-aside_block-item">
                        <a href="/wiki/<?= htmlspecialchars($SIMILAR_CONTENT['articles'][$i]['full_title']); ?>"><?= $SIMILAR_CONTENT['articles'][$i]['title'] . ' - ' . $SIMILAR_CONTENT['articles'][$i]['source_name']?></a>
                        <p><?= $SIMILAR_CONTENT['articles'][$i]['preview']; ?>...</p>
                    </div>
                <? endfor; ?>
                <? if ($SIMILAR_CONTENT['total_count'] > 3):?>
                    <p><a href="<?=Url::to(["search/" . $PAGE_CONTENT['page_main_name']])?>">Показать все</a></p>
                <? endif; ?>
            </div>
        <? } //endif; ?>
        <? if (is_array($OTHER_CONTENT) && isset($OTHER_CONTENT['count']) && $OTHER_CONTENT['count'] > 0) { ?>
            <div class="main_content-aside_block">
                <h3>Другие результаты поиска</h3>
                <? for ($i = 0; $i < 3 && $i < (int)$OTHER_CONTENT['count']; $i++): ?>
                    <div class="main_content-aside_block-item">
                        <a href="/wiki/<?= htmlspecialchars($OTHER_CONTENT['articles'][$i]['full_title']); ?>"><?= $OTHER_CONTENT['articles'][$i]['title'] . ' - ' . $OTHER_CONTENT['articles'][$i]['source_name']?></a>
                        <p><?= $OTHER_CONTENT['articles'][$i]['preview']; ?>...</p>
                    </div>
                <? endfor; ?>
                <? if ($OTHER_CONTENT['total_count'] > 3):?>
                    <p><a href="<?= Url::to(["search/" . \Yii::$app->getRequest()->get('q')])?>">Показать все</a></p>
                <? endif; ?>
            </div>
        <? } //endif; ?>
    </aside>
</article>
<? if ($RANDOM_PAGES['count'] > 0) { ?>
    </section>
    <section class="main_content-additional jumbo_notification bs-callout">
        <div class="jumbotron">
        <p>Пожалуйста, напишите нам о&nbsp;любой ошибке или&nbsp;проблеме в&nbsp;работе энциклопедии. Нам важен каждый&nbsp;отзыв!</p>
        <div class="personal_filters-controls-buttons">
                <a href="http://feedback.eto.global/create" target="_blank"><button type="submit" class="btn btn-primary">Написать отзыв</button></a>
        </div>
        </div>
    </section>
    <section class="main_content-additional">
        <h2>Интересные статьи</h2>
        <div class="rows">
            <? foreach($RANDOM_PAGES['articles'] as $art):?>
                <article class="main_content-item-small bordered">
                    <a href="/wiki/<?= htmlspecialchars($art['full_title'])?>"><picture style="background-image: url('/<?=$art['picture']?>')"> <img src="/<?=$art['picture']?>"> </picture></a>
                    <h4><a href="/wiki/<?= htmlspecialchars($art['full_title'])?>"><?= $art['title']?></a></h4>
                    <a href="/wiki/<?= htmlspecialchars($art['full_title'])?>"><p><?= $art['preview']?>...</p></a>
                </article>
            <? endforeach; ?>
        </div>
<? } //endif; ?>