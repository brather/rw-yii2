<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 05.07.2017
 * Time: 11:01
 */
use yii\widgets\LinkPager;

$query = \Yii::$app->getRequest()->get('query');
?>

<h2>Поиск</h2>
<div class="search_result">
    <? if ($RESULT['count'] !== 0): ?>
        <? foreach ($RESULT['results'] as $item ): ?>
        <div class="search_result-item">
            <h4><a href="<?= '/wiki/' . $item['page_link'] . '?q=' . $query;?>"><?= ($item['highlighted_title']) ? implode('', $item['highlighted_title']) : $item['page_title']?></a></h4>
            <p><?= implode('', $item['highlighted_text']); ?></p>
            <div class="search_result-source">
                <?=$item['page_source_name']?>
            </div>
        </div>
        <? endforeach; ?>
        
        <!--pagination-->
        <nav class="search_result-pagination">
            <?= LinkPager::widget(['pagination' => $PAGES]); ?>
		  <!--<ul class="pagination">
		    <li>
		      <a href="#" aria-label="Previous">
		        <span aria-hidden="true">&laquo;</span>
		      </a>
		    </li>
		    <li><a href="#">1</a></li>
		    <li><a href="#">2</a></li>
		    <li><a href="#">3</a></li>
		    <li><a href="#">4</a></li>
		    <li><a href="#">5</a></li>
		    <li>
		      <a href="#" aria-label="Next">
		        <span aria-hidden="true">&raquo;</span>
		      </a>
		    </li>
		  </ul>-->
		  <p>Всего найдено: <?= $RESULT['total']?></p>
		</nav>
        
        
    <? else: ?>
        <h3>Нет результатов поиска по вашему запросу. Попробуйте сформулировать запрос иначе.</h3>
    <? endif; ?>
	<!--<div class="search_result-item">
		<h4><a href="#">Гагарин Юрий Алексеевич</a></h4>
		<p>ГАГАРИН Юрий Алексеевич (1934-1968). Летчик-космонавт СССР № 1, первый космонавт планеты Земля.Герой Советского Союза.</p>
		<div class="search_result-source">
			<a href="#">Ефимов А. Н., Попович П. Р. Авиационная энциклопедия в лицах. - Москва : Барс, 2007</a>
		</div>
		<div class="other_sources-toggler" data-active-toggle><span>Статьи из других источников</span></div>
		<div class="other_sources-display">
			<p>В 1960 г. старший лейтенант Ю. Гагарин был зачислен в Отряд космонавтов. С 1961 г. был его командиром.</p>
			<div class="other_sources-source">
				<a href="#">Ефимов А. Н., Попович П. Р. Авиационная энциклопедия в лицах. - Москва : Барс, 2007</a>
			</div>
			<p>В 1960 г. старший лейтенант Ю. Гагарин был зачислен в Отряд космонавтов. С 1961 г. был его командиром.</p>
			<div class="other_sources-source">
				<a href="#">Ефимов А. Н., Попович П. Р. Авиационная энциклопедия в лицах. - Москва : Барс, 2007</a>
			</div>
		</div>
	</div>-->
</div>