<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 04.07.2017
 * Time: 12:45
 *
 * @var Array $meta
 * @var String $title
 */

$this->title = $meta['title'] ? : $title;
$this->metaTags = $meta;
?>

<h1><?= $title; ?></h1>
<?= $text; ?>