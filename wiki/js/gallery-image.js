/**
 * Created by AChechel on 06.07.2017.
 */
(function () {
    $('.main_content-article_content').magnificPopup({
        delegate: '.image',
        type: 'image',
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        }
    });
})();